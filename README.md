# README #

### Links útiles ###

http://physics.princeton.edu/~phy209/week5/

http://bigtheta.io/2016/02/29/the-ising-model.html

http://rajeshrinet.github.io/blog/2014/ising-model/

### Para correr Ising2dWolff.cpp ###

Es necesario hacer lo siguiente:

sudo apt-get install libboost-all-dev

Y para compilarlo

g++ Ising2DWolff.cpp -lstdc++ -o test

###IMPORTANTE###

La manera en que se crean los datos y se entrena es la siguiente

En main.py, en la última línea se debe poner:

main(L, 25000, numlists=30)

donde L es el tamaño del modelo, se hacen 25000 muestras para cada temperatura, y hay numlists temperaturas.

Después en learning.py en la última línea se debe poner:

train(L, 'both')

donde es el mismo tamaño del modelo.

La carpeta 'resultados' debe ser pusheada siempre, mientras que 'files' no, para no llenar el repositorio de basura.

**Primero se corre main.py y después learning.py**
