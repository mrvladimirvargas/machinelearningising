//Ising 2D por Wolff
#include<cmath>
#include<iostream>
#include "Ising2DWolff.h"
using namespace std;

int main(){
  SpinSystem Ising;
  Ising.set_values(8, 2.0, 1000, 0);
  Crandom ran2(Ising.semilla);
  
  int mcs,t,ccorr,c;
  double Mprom,M2prom,M4prom,Eprom,E2prom,E;
  double Cv,Xs,Ub;
  double T,p;  
  //PARA CALCULAR LOS VALORES MEDIOS EN FUNCION DE LA TEMPERATURA
  double M;
  for(T=1.0;T<4.0;T+=0.1){
    p=1-exp(-(2*Ising.J)/(Ising.k*T));
    //Inicio el sistema
    Ising.InicieArriba();
    //Equilibro
    for(t=0;t<Ising.teq;t++){
      for(mcs=0;mcs<Ising.L2;) //Un MCSS
	mcs+=Ising.WolffCluster(ran2,p);
    }
    //Inicio Acumuladores en cero
    Mprom=M2prom=M4prom=Eprom=E2prom=0;
    //Tomo muestras
    for(t=0;t<Ising.Nmuestras;t++){
      E=Ising.GetE();  M=Ising.GetM();
      Mprom+=M; M2prom+=M*M; M4prom+=M*M*M*M; Eprom+=E; E2prom+=E*E;
      for(ccorr=0;ccorr<Ising.tcorr;ccorr++)
	for(mcs=0;mcs<Ising.L2;) //Un MCSS
	mcs+=Ising.WolffCluster(ran2,p);
    }
    //Normalizo los acumuladores
    Mprom/=Ising.Nmuestras; M2prom/=Ising.Nmuestras; M4prom/=Ising.Nmuestras;
    Eprom/=Ising.Nmuestras; E2prom/=Ising.Nmuestras;
    //Calculo lo que me interesa
    Cv=1/(Ising.k*T*T)*(E2prom-Eprom*Eprom);
    Xs=1/(Ising.k*T)*(M2prom-Mprom*Mprom);
    Ub=1.0-1.0/3.0*(M4prom/(M2prom*M2prom));
    //Imprimo
    cout<<T<<" "<<Eprom<<" "<<Mprom<<" "<<Cv<<" "<<Xs<<" "<<Ub<<" "<<M2prom<<endl;
  }//*/
  
  return 0;
}
