import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import matplotlib.pylab as plt

data3 = np.loadtxt('20_3_25000_3.dat', dtype=np.dtype(int))
data1 = np.loadtxt('20_1_25000_1.dat', dtype=np.dtype(int))
data2 = np.loadtxt('20_2_25000_2.dat', dtype=np.dtype(int))
data4 = np.loadtxt('20_4_25000_4.dat', dtype=np.dtype(int))
pca = PCA(n_components=2)
tsne = TSNE(n_components=2)
#alldata = pca.fit(np.vstack((data1, data2, data3, data4)))
#data1 = pca.transform(data1)
#data2 = pca.transform(data2)
#data3 = pca.transform(data3)
#data4 = pca.transform(data4)
number = 4500
alldata = tsne.fit_transform(np.vstack((data1[np.random.choice(data1.shape[0], number, replace=False), :], data2[np.random.choice(data2.shape[0], number, replace=False), :], data3[np.random.choice(data3.shape[0], number, replace=False), :], data4[np.random.choice(data4.shape[0], number, replace=False), :])))
data1 = alldata[:number, :]
data2 = alldata[number:2*number, :]
data3 = alldata[2*number:3*number, :]
data4 = alldata[3*number:4*number, :]
plt.plot(data1[:, 0], data1[:, 1], 'r.')
plt.plot(data2[:, 0], data2[:, 1], marker='.', color='orange', lw=0)
plt.plot(data3[:, 0], data3[:, 1], marker='.', color='turquoise', lw=0)
plt.plot(data4[:, 0], data4[:, 1], marker='.', color='green', lw=0)
plt.show()