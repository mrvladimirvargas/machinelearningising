//Ising 2D por Wolff

#include<cmath>
#include<iostream>
#include "boost/multi_array.hpp"
#include "Random64.h"

class SpinSystem{
public:
  int L;
  int L2;
  int k, J;
  int semilla;
  int teq, tcorr;
  double TC;//TC=2.269;
  double Temp;
  int Nmuestras;
  boost::multi_array<int, 2> s; 
  int E,M; 
  boost::multi_array<int, 2> Cluster; 
  int write;
  void set_values(int, double, int, int);
  void InicieArriba(void);
  void Muestre(void);
  void MuestreCluster(void);
  bool NoEstaEnElCLuster(int i,int j);
  int WolffCluster(Crandom & ran2,double p);
  double GetE(void){return (double)E;};
  double GetM(void){return fabs((double)M);};
};

void SpinSystem::set_values(int length, double temperature, int nsamples, int seed){
  L = length;
  Temp = temperature;
  Nmuestras = nsamples;
  L2 = length*length;
  teq = (int)(10*pow(length/8.0,0.58));
  tcorr = (int)(1*pow(length/8.0,0.58));
  k=1; J=1;
  TC=2/log(1+sqrt(2.0));
  s.resize(boost::extents[L][L]);
  Cluster.resize(boost::extents[L2][2]);
  semilla = seed;
}
void SpinSystem::InicieArriba(void){
  for(int i=0;i<L;i++)
    for(int j=0;j<L;j++)
      s[i][j]=1;
  M=L2; E=-2*L2;
  write=0;
}
void SpinSystem::Muestre(void){
  for(int i=0;i<L;i++){
    for(int j=0;j<L;j++)
      cout<<s[i][j]<<" ";
    //cout<<endl;
  }
  //cout<<"M="<<M<<" ; E="<<E<<endl<<endl;
}
bool SpinSystem::NoEstaEnElCLuster(int i,int j){
  int search; bool NoEsta=true;
  for(search=0;search<write;search++)
    if(Cluster[search][0]==i && Cluster[search][1]==j){
      NoEsta=false; break;
    }
  return NoEsta;
}

void SpinSystem::MuestreCluster(void){
  int i,j,search; bool NoEsta;
  for(i=0;i<L;i++){
    for(j=0;j<L;j++){
      if(NoEstaEnElCLuster(i,j))
    cout<<"0 ";
      else
    cout<<"1 ";
    }cout<<endl;
  }
  cout<<"Tamaño del Cluster="<<write<<endl<<endl;
}
int SpinSystem::WolffCluster(Crandom & ran2,double p){
  int n,i,j,south,north,east,west,dE,sref,read;
  //Escojo el primer espín, y lo añado a la lista de leer;
  write=read=0;
  n=(int) (L2*ran2.r()); i=n/L; j=n%L;
  sref=s[i][j]; Cluster[write][0]=i; Cluster[write][1]=j; write++;
  while(read<write){
    //voltearlo
    i=Cluster[read][0];  j=Cluster[read][1];
    south=(i+1)%L; north=(i+L-1)%L; east=(j+1)%L; west=(j+L-1)%L; 
    dE=2*s[i][j]*(s[south][j]+s[north][j]+s[i][east]+s[i][west]);
    s[i][j]*=-1; E+=dE; M+=2*s[i][j];
    read++;
    //introducir nuevos en la lista 
    //North
    if(s[north][j]==sref) //Si está en la misma dirección,
      if(ran2.r()<p) //y los dados nos favorecen para agregarlo,
    if(NoEstaEnElCLuster(north,j)){//y no está en el cluster
      Cluster[write][0]=north; Cluster[write][1]=j; write++; //agregarlo.
    }    
    //South
    if(s[south][j]==sref) //Si está en la misma dirección,
      if(ran2.r()<p) //y los dados nos favorecen para agregarlo,
    if(NoEstaEnElCLuster(south,j)){//y no está en el cluster
      Cluster[write][0]=south; Cluster[write][1]=j; write++; //agregarlo.
    }
    //East
    if(s[i][east]==sref) //Si está en la misma dirección,
      if(ran2.r()<p) //y los dados nos favorecen para agregarlo,
    if(NoEstaEnElCLuster(i,east)){//y no está en el cluster
      Cluster[write][0]=i; Cluster[write][1]=east; write++; //agregarlo.
    }
    //West
    if(s[i][west]==sref) //Si está en la misma dirección,
      if(ran2.r()<p) //y los dados nos favorecen para agregarlo,
    if(NoEstaEnElCLuster(i,west)){//y no está en el cluster
      Cluster[write][0]=i; Cluster[write][1]=west; write++; //agregarlo al culster  
    }
  }
  return write;
}