#include<cmath>
#include<iostream>
#include "Ising2DWolff.h"
using namespace std;
int main(){
  SpinSystem Ising;
  Ising.set_values(20 , 3, 100000, 0);
  Crandom ran2(Ising.semilla);
  double p;
  int mcs,t,ccorr,c;
  p = 1-exp(-(2*Ising.J)/(Ising.k*Ising.Temp));
  //Inicio el sistema
  Ising.InicieArriba();
  //Equilibro
  for(t=0;t<Ising.teq;t++){
    for(mcs=0;mcs<Ising.L2;mcs++) //Un MCSS
      mcs+=Ising.WolffCluster(ran2,p);
  }
  // Toma de muestras
  for(t=0;t<Ising.Nmuestras;t++){
    Ising.Muestre();
    cout << endl;
    for(ccorr=0;ccorr<Ising.tcorr;ccorr++){
      for(mcs=0;mcs<Ising.L2;) //Un MCSS
        mcs+=Ising.WolffCluster(ran2,p);
    }
  }
  return 0;
}