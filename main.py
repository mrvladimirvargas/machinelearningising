import subprocess
import multiprocessing as mp
import numpy as np
import time
import os
from shutil import copy2

def create_file(length, temperature, nsamples, seed, label):
    with open("template.cpp", "r") as f:
        contents = f.read()
    contents = contents.replace("LENGTH",  str(length)).replace("TEMP", str(temperature)).replace("NSAMPLES", str(nsamples)).replace("SEED", str(seed))
    with open("./files/{0}.cpp".format(label), "w") as f:
        f.write(contents)

def create_job(in_list):
    length, temperature, nsamples, seed = in_list
    label = "_".join([str(el) for el in in_list])
    create_file(length, temperature, nsamples, seed, label)
    subprocess.call('g++ ./files/{0}.cpp -lstdc++ -o ./files/test{0}'.format(label), shell=True)
    subprocess.call('./files/test{0} > ./files/{0}.dat'.format(label), shell=True)



Tc = 2.2693
def generate_lists(L, nsamples, Tcritic = Tc, numlists=10, mintemp = 1, maxtemp = 4):
    if numlists%2 !=0:
        numlists-=1
    if mintemp >= Tc or maxtemp <= Tc:
        raise ValueError('Min(max) temp {0} ({1}) is greater (less) than Critical temperature {2}'.format(mintemp, maxtemp, Tcritic))
    Tstep = (maxtemp-mintemp)/numlists
    temps = (np.arange(numlists))/(numlists-1)*(maxtemp-mintemp)+mintemp
    meant = np.mean(temps)
    temps = temps -meant + Tcritic
    for seed in range(numlists):
        yield (L, temps[seed], nsamples, seed)

def main(L, nsamples, Tcritic = Tc, numlists=10, mintemp = 1, maxtemp = 4):
    starttime = time.time()
    processes = 4
    pool = mp.Pool(processes=processes)
    results = pool.map(create_job, generate_lists(L, nsamples, Tcritic, numlists, mintemp, maxtemp))
    pool.close()
    pool.join()
    finishtime = time.time()
    totaltime = finishtime - starttime
    with open("./resultados/report{0}_creation.txt".format(L), "w") as f:
        f.write("Total time was {0} mins\n".format(totaltime/60))
        f.write("Parameters were:\nL: {0}\nnsamples: {1}\ncritical temperature: {2}\nnumber of temperatures: {3} from {4} to {5}".format(L, nsamples, Tcritic, numlists, mintemp, maxtemp))

if __name__ == '__main__':
    try:
        try:
            os.mkdir('resultados')
        except:
            pass
        try:
            os.mkdir('files')
        except:
            pass
        copy2('Ising2DWolff.h', 'files')
        copy2('Random64.h', 'files')
    except:
        pass
    main(40, 25000, numlists=30)