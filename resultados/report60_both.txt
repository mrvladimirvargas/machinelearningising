Selected model: Both SVM and NN
Distribution of configurations by temperature:
T=2.11412758621: 6000 configurations
T=1.07964482759: 5199 configurations
T=1.18309310345: 5912 configurations
T=1.38998965517: 6000 configurations
T=2.52792068966: 6000 configurations
T=2.42447241379: 6000 configurations
T=1.90723103448: 6000 configurations
T=1.28654137931: 5999 configurations
T=0.7693: 64 configurations
T=2.73481724138: 6000 configurations
T=3.14861034483: 6000 configurations
T=3.56240344828: 6000 configurations
T=3.35550689655: 6000 configurations
T=3.25205862069: 6000 configurations
T=3.66585172414: 6000 configurations
T=1.80378275862: 6000 configurations
T=1.5968862069: 6000 configurations
T=3.7693: 6000 configurations
T=2.83826551724: 6000 configurations
T=0.872748275862: 745 configurations
T=2.21757586207: 6000 configurations
T=0.976196551724: 3020 configurations
T=2.63136896552: 6000 configurations
T=2.32102413793: 6000 configurations
T=2.9417137931: 6000 configurations
T=3.45895517241: 6000 configurations
T=1.70033448276: 6000 configurations
T=3.04516206897: 6000 configurations
T=1.49343793103: 6000 configurations
T=2.01067931034: 6000 configurations
Paramagnetic configurations: 90000
Ferromagnetic configurations: 74939
Plot files created
NN precision: 97.97943424158765%
NN time: 2.13680393298467 mins
SVM precision: 99.56254050550875%
SVM time: 28.253971028327943 mins