Selected model: Both SVM and NN
Distribution of configurations by temperature:
T=3.56240344828: 25000 configurations
T=3.04516206897: 25000 configurations
T=1.70033448276: 7145 configurations
T=0.872748275862: 2 configurations
T=3.25205862069: 25000 configurations
T=2.63136896552: 24996 configurations
T=3.35550689655: 25000 configurations
T=1.38998965517: 987 configurations
T=2.01067931034: 18573 configurations
T=2.21757586207: 23439 configurations
T=0.7693: 2 configurations
T=2.11412758621: 21439 configurations
T=3.7693: 25000 configurations
T=3.14861034483: 25000 configurations
T=2.9417137931: 25000 configurations
T=1.28654137931: 408 configurations
T=2.32102413793: 24451 configurations
T=1.07964482759: 104 configurations
T=1.49343793103: 2058 configurations
T=2.52792068966: 24970 configurations
T=1.18309310345: 236 configurations
T=2.42447241379: 24858 configurations
T=1.5968862069: 4079 configurations
T=2.83826551724: 24999 configurations
T=1.90723103448: 14732 configurations
T=1.80378275862: 10824 configurations
T=3.66585172414: 25000 configurations
T=0.976196551724: 27 configurations
T=3.45895517241: 25000 configurations
T=2.73481724138: 24998 configurations
Paramagnetic configurations: 374272
Ferromagnetic configurations: 104055
Plot files created
NN precision: 90.47976011994004%
NN time: 0.8049380620320638 mins
SVM precision: 93.71321306989915%
SVM time: 28.004170723756154 mins