import numpy as np
import os
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import precision_score
from sklearn import svm
from sklearn.manifold import TSNE
import matplotlib.pylab as plt
import pickle
import time

def open_files(L, nsamples=None):
    files = []
    ferro_temps = []
    para_temps = []
    for file in os.listdir('files'):
        if file.endswith('.dat') and file.startswith(str(L)):
            files.append(os.path.join('files', file))
    configsdict = {}
    for file in files:
        spinconfigs = np.loadtxt(file, dtype=np.dtype(int))
        spinconfigs = np.unique(spinconfigs, axis=0)
        if nsamples:
            spinconfigs = spinconfigs[np.random.choice(spinconfigs.shape[0], nsamples, replace=False), :]
        temp = file.split('_')[1]
        configsdict[temp] = np.column_stack(([float(temp)]*spinconfigs.shape[0], spinconfigs))
        if float(temp) < 2.2693:
            ferro_temps.append(temp)
        else:
            para_temps.append(temp)
    return configsdict, ferro_temps, para_temps

def plot_reduction(X_scaled, y, L, maxsamples=2000, plot=True):
    if maxsamples > 2000:
        raise ValueError('maxsamples should not be larger than 2000')
    separation = int(X_scaled.shape[0]/2)
    ferros = X_scaled[:separation]
    paras = X_scaled[separation:]
    ferros = ferros[np.random.choice(ferros.shape[0], maxsamples, replace=False), :]
    paras = paras[np.random.choice(paras.shape[0], maxsamples, replace=False), :]
    allconfigs = np.vstack((ferros, paras))
    tsne = TSNE(n_components=2)
    reduced = tsne.fit_transform(allconfigs)
    ferros_red = reduced[:maxsamples]
    paras_red = reduced[maxsamples:]
    with open("./resultados/{0}_ferros.p".format(L), "wb") as f:
        pickle.dump(ferros_red, f)
    with open("./resultados/{0}_ferros.p".format(L), "wb") as f:
        pickle.dump(ferros_red, f)
    if plot:
        plt.scatter(ferros_red[:,0], ferros_red[:,1], c='red', alpha=0.2)
        plt.scatter(paras_red[:,0], paras_red[:,1], c='green', alpha=0.2)
        plt.title(r"TSNE visualization for $L={0}$".format(L))
        plt.savefig('./resultados/TSNEvis_{0}.pdf'.format(L), dpi=300)
        #plt.show()

def classify(clf, X_train, y_train, X_test, y_test, L, name):
    t_test = X_test[:, 0]
    X_train = X_train[:, 1:]
    X_test = X_test[:, 1:]
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    with open("./resultados/{0}_trains_{1}.p".format(L, name), "wb") as f:
        test = np.column_stack((t_test, y_test, y_pred))
        pickle.dump(test, f)
        np.savetxt("./resultados/{0}_trains_{1}.dat".format(L, name), test)
    with open("./resultados/{0}_model_{1}.p".format(L, name), "wb") as f:
        pickle.dump(clf, f)
    return precision_score(y_test, y_pred)
        
def train(L, model, plot=True, nsamples=None, maxsamples=2000):
    report = []
    if model=='nn':
        report.append('Selected model: Neural Network')
        clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(100,), random_state=1)
        clf_name = 'MM'
    elif model=='svm':
        clf = svm.SVC(random_state=1)
        clf_name = 'SVM'
        report.append('Selected model: Support Vector Machine')
    elif model=='both':
        report.append('Selected model: Both SVM and NN')
    else:
        raise ValueError('model should be "nn", "svm" or "both".')
    spinconfigs, ferro_temps, para_temps = open_files(L, nsamples=nsamples)
    ferro_configs = np.unique(np.vstack((spinconfigs[t] for t in ferro_temps)), axis=0)
    para_configs = np.unique(np.vstack((spinconfigs[t] for t in para_temps)), axis=0)
    report.append('Distribution of configurations by temperature:')
    for t, sc in spinconfigs.items():
        report.append('T={0}: {1} configurations'.format(t, sc.shape[0]))
    report.append('Paramagnetic configurations: {0}\nFerromagnetic configurations: {1}'.format(para_configs.shape[0], ferro_configs.shape[0]))
    del spinconfigs, ferro_temps, para_temps
    if ferro_configs.shape[0] < para_configs.shape[0]:
        para_configs = para_configs[np.random.choice(para_configs.shape[0], ferro_configs.shape[0], replace=False), :]
    else:
        ferro_configs = ferro_configs[np.random.choice(ferro_configs.shape[0], para_configs.shape[0], replace=False), :]
    X = np.vstack((ferro_configs, para_configs))
    y = [0]*ferro_configs.shape[0] + [1]*para_configs.shape[0]
    del ferro_configs, para_configs
    X_scaled = np.column_stack((X[:, 0], preprocessing.scale(X[:, 1:]) ))
    plot_reduction(X_scaled[:, 1:], y, L, maxsamples=maxsamples, plot=plot)
    report.append('Plot files created')
    X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.33, random_state=42)
    del X, y, X_scaled
    if model=='both':
        start_time = time.time()
        clf = MLPClassifier(solver='adam', alpha=1e-5, hidden_layer_sizes=(100,), random_state=1)
        precisionNN = classify(clf, X_train, y_train, X_test, y_test, L, 'NN')
        finish_time = time.time()
        report.append('NN precision: {0}%'.format(precisionNN*100))
        report.append('NN time: {0} mins'.format((finish_time - start_time)/60))
        start_time = time.time()
        clf = svm.SVC(random_state=1)
        precisionSVM = classify(clf, X_train, y_train, X_test, y_test, L, 'SVM')
        finish_time = time.time()
        report.append('SVM precision: {0}%'.format(precisionSVM*100))
        report.append('SVM time: {0} mins'.format((finish_time - start_time)/60))
    else:
        start_time = time.time()
        precision = classify(clf, X_train, y_train, X_test, y_test, L, clf_name)
        finish_time = time.time()
        report.append('Model precision: {0}%'.format(precision*100))
        report.append('Model time: {0} mins'.format((finish_time - start_time)/60))
    with open("./resultados/report{0}_{1}.txt".format(L, model), "w") as f:
        f.write("\n".join(report))

if __name__ == '__main__':
    train(40, 'both')